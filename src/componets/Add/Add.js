import React, {Component} from 'react';
import './NoteAdd.css';
import axios from 'axios';


class NoteAdd extends Component {

    state = {
        post: {},
        title: '',
        body: '',
        loading: false
    };

    itemCreate = event => {
        event.preventDefault();
        this.setState({loading: true});

        let post = {
            title: this.state.title,
            body: this.state.body,
            date: Date()
        };
        axios.post('/items.json', post).then(() => {
            this.setState({loading: false});

            this.props.history.replace('/');
        });

        this.setState({post});
        console.log(this.state.post);
    };

    noteValueChanged = event => {
        event.persist();

        this.setState(prevState => {
            console.log(event.target.value);
            return {
                post: {[event.target.name]: event.target.value}

            }
        })
    };

    render() {
        // console.log(this.state.post);

        return (

            <form className="NoteAdd Container">
                <input type="text" name="title" placeholder="New note title" value={this.state.title}
                       onChange={this.noteValueChanged}/>
                <textarea name="body" placeholder="Enter your notes..." value={this.state.body}
                          onChange={this.noteValueChanged}/>
                <button onClick={this.itemCreate}>Add</button>
            </form>
        )
    }


}

export default NoteAdd;