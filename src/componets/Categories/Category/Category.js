import React from 'react';
import './Category.css';

const Category = props => {
    return (
        <li className='CatName' onClick = {() => props.getJoke(props.name)}>
            {props.name}
        </li>
    )
};

export default Category;