import React from 'react';
import Category from "./Category/Category";
import './Categories.css';


const Categories = props => {
    return (
        <ul className="Categories">
            {props.categories.map(category => <Category name={category} key={category} getJoke={props.getJoke}/>)}
        </ul>
    )
};

export default Categories;
