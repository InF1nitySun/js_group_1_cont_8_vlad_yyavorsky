import React from 'react';
import {NavLink} from 'react-router-dom';
import './Navigation.css';

const Navigation = props => {
    return (
        <div className="Navigation ">
            <nav className="Nav Container">
                <a href="/" title="Домашняя работа 63" alt="home" className="Logos"> </a>
                <ul>
                    <li>
                        <NavLink to="/"exact >HomePage</NavLink>
                    </li>
                    <li>
                        <NavLink to="/add" >Add</NavLink>
                    </li>
                    <li>
                        <NavLink to="/about">About</NavLink>
                    </li>
                    <li>
                        <NavLink to="/contact">Contacts</NavLink>
                    </li>
                </ul>
            </nav>
        </div>
    )
};

export default Navigation;